package id.java.perpustakaan.delivery;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.java.perpustakaan.model.Buku;
import id.java.perpustakaan.model.Request;
import id.java.perpustakaan.model.Update;
import id.java.perpustakaan.services.BukuService;

@RestController
@RequestMapping("Perpustakaan")
public class PerpusController {
    

    @Autowired
    BukuService bukuService;

    @GetMapping("/All")
    public List<Buku> GetList(@RequestParam(defaultValue = "0") int page,
                              @RequestParam(defaultValue = "10") int size){
        return bukuService.GetList(page, size);
    }

    @GetMapping("/Buku/{id}")
    public Buku GetById(@PathVariable int id){
        return bukuService.GetById(id);
    }

    @PutMapping("/Buku/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void Update(@PathVariable int id, @RequestBody Update data){
        bukuService.Update(id, data);
    }
    

    @PostMapping("/AddBuku")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void Add(@RequestBody Request data){
        bukuService.Add(data);  
    }

    @DeleteMapping("/Delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void Delete(@PathVariable int id){
        bukuService.Delete(id);
    }
}

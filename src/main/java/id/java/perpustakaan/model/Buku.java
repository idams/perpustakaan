package id.java.perpustakaan.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table
@Entity
public class Buku {

    @Id
    @GeneratedValue
    private int Id;
    @Column 
    private String Judul;
    @Column
    private String Kategori;
    @Column
    private String Pengarang;
    @Column
    private String Penerbit;
    
    public Integer getId() {
        return Id;
    }
    public void setId(Integer id) {
        Id = id;
    }
    public String getJudul() {
        return Judul;
    }
    public void setJudul(String judul) {
        Judul = judul;
    }
    public String getKategori() {
        return Kategori;
    }
    public void setKategori(String kategori) {
        Kategori = kategori;
    }
    public String getPengarang() {
        return Pengarang;
    }
    public void setPengarang(String pengarang) {
        Pengarang = pengarang;
    }
    public String getPenerbit() {
        return Penerbit;
    }
    public void setPenerbit(String penerbit) {
        Penerbit = penerbit;
    }


    
    
}

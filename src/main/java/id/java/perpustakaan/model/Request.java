package id.java.perpustakaan.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Request {

    @JsonProperty("Judul")
    private String Judul;
    @JsonProperty("Kategori")
    private String Kategori;
    @JsonProperty("Pengarang")
    private String Pengarang;
    @JsonProperty("Penerbit")
    private String Penerbit;    
}

package id.java.perpustakaan.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Update {
    @JsonProperty("Kategori")
    private String Kategori;
    @JsonProperty("Penerbit")
    private String Penerbit;

    
}

package id.java.perpustakaan.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.java.perpustakaan.model.Buku;
import java.util.List;


public interface BukuRepository extends JpaRepository<Buku, Integer>{

    @Query(value="select * from buku where judul = :judul", nativeQuery = true)
    List<Buku> findByJudul(@Param("judul") String judul);
    
}

package id.java.perpustakaan.services;

import java.util.List;

import id.java.perpustakaan.model.Buku;
import id.java.perpustakaan.model.Request;
import id.java.perpustakaan.model.Update;

public interface BukuService {


    List<Buku> GetList(int page, int size);
    Buku GetById(int id);
    void Add(Request request);
    void Update(int id, Update request);
    void Delete(int id);
    
}

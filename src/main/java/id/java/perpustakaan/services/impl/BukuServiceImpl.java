package id.java.perpustakaan.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import id.java.perpustakaan.model.Buku;
import id.java.perpustakaan.model.Request;
import id.java.perpustakaan.model.Update;
import id.java.perpustakaan.repository.BukuRepository;
import id.java.perpustakaan.services.BukuService;

@Service
public class BukuServiceImpl implements BukuService{

    @Autowired
    BukuRepository bukuRepository;
    
    @Override
    public List<Buku> GetList(int page, int size){
        PageRequest pageRequest = PageRequest.of(page, size);
        return bukuRepository.findAll(pageRequest).getContent();
    }
    
    @Override
    public Buku GetById(int id){
        return bukuRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public void Add(Request request){
        Buku book = new Buku();
        book.setJudul(request.getJudul());
        book.setKategori(request.getKategori());
        book.setPenerbit(request.getPenerbit());
        book.setPengarang(request.getPengarang());
        bukuRepository.save(book);
    }


    @Override
    public void Update(int id, Update request){
        Buku book = GetById(id);
        book.setKategori(request.getKategori());
        book.setPenerbit(request.getPenerbit());
        bukuRepository.save(book);
    }

    @Override
    public void Delete(int id){
        GetById(id);
        bukuRepository.deleteById(id);
    }
}

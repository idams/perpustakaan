package id.java.perpustakaan.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import id.java.perpustakaan.model.Request;
import id.java.perpustakaan.repository.BukuRepository;
import id.java.perpustakaan.services.BukuService;

@SpringBootTest
@EnabledOnOs({OS.WINDOWS})
public class BukuServiceTest {
    
    @Autowired
    BukuService bukuService;

    @Autowired
    BukuRepository bukuRepository;

    @Test
    void Add(){
    Request req = new Request();    
    req.setPenerbit("abcd");
    req.setJudul("abcd");
    req.setPenerbit("abcd");
    req.setPengarang("abcd");
    bukuService.Add(req);

    Assertions.assertNotNull(bukuRepository.findByJudul("abcd"));

    }


}
